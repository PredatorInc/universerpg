UniverseRPG
===========

A work-in-progress RPG made by Predator Games
Developed by: Gabriel Ashcraft (sonicdude5787 on BitBucket) and Ryan Wilson (PotatoBorn on BitBucket)

Special thanks to Frez, Duckd0nald and all the others who beta test this RPG


This game requires a free account to be played. Get one from the developers, who are named at the beginning of this README document.

The game has many features, including in-game music and ASCII art. Some art even has animation to complement it!

This game pushes the boundaries of Batch to the point where it is not gone before. It has ASCII art with animation, in-game music, partial color, and much more. If you want a good demonstration of what an old, nearly out of date programming language can do, download this game and play it.

A wonderful and mind-boggling story mode is coming soon.. be prepared for the Storytelling Update (version R9), coming soon!

How to Install:

1. This is only compatible on a PC that can use batch files. The only one we know of is Windows.

2. Download the Universe RPG installer from our website at universerpgblog.blogspot.com

3. Go through the installation prompts.

4. Open UniverseGame.bat (in the folder where you installed Universe), and game on!

How to Modify:

1. Install Git and configure it. (If you already have Git, of course, skip this step and go to step 2.)

2. Fork the UniverseRPG repository from BitBucket at https://bitbucket.org/PredatorInc/universerpg

3. Clone your fork in Git.

4. Modify anything in a good text editor, preferably Notepad++, which is a free open source text editor meant for programming.

5. Commit your modifications to your fork in Git, and push it to your fork in Git.

6. Send us a pull request, and we'll check it out. You might be credited by your BitBucket username if we add your modifications!

Note that the game itself is made with Batch, an old programming language that is not very advanced (it's easy to learn, trust me), but has lots of potential. It's been around since the days of the MS-DOS text-based operating system. Learn it, and then try to send us a pull request. We don't want any bugs in our game.

---THE UN-FINISHED STORY MODE IS OUT! TRY IT NOW WITH A QUICK DOWNLOAD/UPDATE OF OUR GAME!---