@echo off
TITLE Universe R8.1.0
setlocal enabledelayedexpansion

:loading
cls
echo Loading..
echo Loading Predator Engine...
echo Loading PredatorEngine.Decryptionfiles...
echo Loading PredatorEngine.RunService...
echo Loading PredatorEngine.ErrorReporter...
echo Loading PredatorEngine.Windows.VERSION4WINDOWS...
echo Loading songs...
echo Loading Bass Guitar Solos...
echo Loading Gamesaves...
echo Pwning Noobs...
echo Loading Epic Bass Drops...
echo Loading art...
echo Loading ErrorReport.Cache
echo Comparing BitBucket repository contents..
echo Loaded successfully.
echo Now bringing you to the login screen..
goto menu

:: ## MAIN MENU ##################################
:menu
cls
color 07
echo          ----UNIVERSE----
echo  UUUUUUUU    UUUUUUUU      UUUUUUUU
echo  UU      UU  UU      UU  UU
echo  UU      UU  UU      UU  UU
echo  UUUUUUUU    UUUUUUUU    UU   UUUUU
echo  UU    UU    UU          UU        UU
echo  UU     UU   UU          UU        UU
echo  UU      UU  UU            UUUUUUUU
echo.
echo.
echo /                         \
echo 1)Begin Game
echo 2)Exit Game
echo 3)The Legendary Chronicle 
echo 4)Credits
echo 5)Music Club (WIP) (Not on DOS)
echo \_________________________/
echo.
set /p c=Enter:

if "%c%" == "4" goto Credits
if "%c%" == "1" goto new
if "%c%" == "2" goto exitgame
if "%c%" == "3" goto news
if "%c%" == "forgottenstory" goto storynew
if "%c%" == "5" goto MusicClub
if "%c%" == "4" goto Credits
goto menu

:: ## NEWS ########################################
:news
cls
echo THE LEGENDARY CHRONICLE
echo Welcome to the Legendary Chronicle, the weekly update newsletter.
echo There are a few sections.
echo 1)Game Updates
echo 2)Game News
echo 3)New Accounts
echo 4)Save File Classifieds
echo 5)Exit to Main Menu
set /p c=
if "%c%" == "1" goto news.gameupdates
if "%c%" == "2" goto news.gamenews
if "%c%" == "3" goto news.newaccounts
if "%c%" == "4" goto news.sfclassifieds
if "%c%" == "5" goto menu
goto news

:news.gameupdates
cls
echo Welcome to Game Updates.
echo Version R8.1.0 - The Partial Storytelling Update
echo Sup, it's Ryan again, and cake
echo - Ryan
echo.
echo Sup guys. It's me again, you probably know who I am by now..
echo This update has an unfinished story mode, but it is completely functional, and has one ending.
echo Note again, the story mode is not finished yet. More will be added upon it soon.
echo Oh, again.. my nickname is Sonichack. Thanks for updating your copy of Universe.
echo - Sonichack
pause
goto news

:news.gamenews
cls
echo Welcome to Game News.
echo This section delivers news for Universe.
echo ISSUE 7 OF THE LEGENDARY CHRONICLE
echo Universe has received most of its new revamp with Ryan on the team.
echo We are no longer using GitHub to deliver updates. You must use BitBucket to get the latest updates to Universe now.
echo The link to our BitBucket repository is https://bitbucket.org/PredatorInc/universerpg/get/3cb41cb99160.zip
echo That's it.
pause
goto news

:news.newaccounts
cls
echo Welcome to New Accounts.
echo This section delivers info about new accounts for Universe.
echo NEW ACCOUNTS CREATED:
echo Badger, Sonichack, PotatoBorn
echo That's it.
pause
goto news

:news.sfclassifieds
cls
echo Welcome to Save File Classifieds.
echo This section delivers classifieds of save files.
echo SAVE FILE CLASSIFIEDS:
echo None created yet.
echo Want to upload a Save File Classified?
echo Ask Gabriel or Ryan to upload it.
echo That's it.
pause
goto news

:: ## NEW GAME ################################

:new
cls
set health=100
set money=20
set bodycount=0
set BB=false
set Pistol=false
set current=Fist
set p=false
echo What is your name?
set /p ya=Name:
set name=!ya!
echo Where is your home located, !name!?
set /p hax=City:
set city=!hax!
echo What is your class, !name!?
echo Assassin, Wizard, Warrior, Samurai, Mage, Battle Mage, Ninja, or Thief?
echo For the game to work properly, you MUST have a capital letter at the beginning of your class name!
echo For example, Assassin
echo Also remember that each class has different stats to start out with.
echo And lastly, there are some secret classes..
set /p derp=Class:
set class=!derp!
goto statdetermine1

:statdetermine1
::Basic Sneak Class
if "!class!" == "Assassin" goto statdeterminea 
::Basic Magic Class
if "!class!" == "Wizard" goto statdeterminew
::Basic Warrior Class
if "!class!" == "Warrior" goto statdeterminewa
::Like A Warrior, but Slower, and more connected to ancient chineese magic
if "!class!" == "Samurai" goto statdeterminesam
::Like a wizard, but less skilled in magic, and faster
if "!class!" == "Mage" goto statdeterminemag
::A secret Over powered class for devs only. If anyone that isn't a dev is caught on savefileclassifieds with this, they will get the classified deleted
if "!class!" == "SECRETDEVCLASS" goto SECRETDEVCLASS
::Like Assassin, But faster, with less damage and mana
if "!class!" == "Ninja" goto statdeterminenin
::Leon Chameleon, here to pwn you
if "!class!" == "Chameleon" goto statdetermineleon
::Ayla is the bomb. She is ready to pwn the weaklings in this universe.
if "!class!" == "Ayla" goto statdetermineayla
::Derpl will rekt you with his nuke of doom.
if "!class!" == "Derpl" goto statdeterminederpl
::A mage who also weilds a sword
if "!class!" == "BattleMage" goto statdetermineBM
::Scrublord
if "!class!" == "TerryPatches" goto statdetermineterry
::Haha, very funny.
if "!class!" == "Ferrari" goto statdetermineferrari
::wow
if "!class!" == "Doge" goto statdeterminedoge

:statdeterminedoge
set playerdmg=1337
set speed=1337
set mana=1337
echo wow such doge many meme
echo how find my secret class
pause
goto home

:statdetermineferrari
set playerdmg=30
set speed=53
set mana=2
echo vroom vroom
pause
goto home

:statdeterminehobb
set playerdmg=7
set speed=3
set mana=4
goto home

:statdetermineterry
set playerdmg=-99999
set speed=-9999
set mana=-99999
echo LOL SCRUBLORD
pause
goto home

:statdeterminederpl
set playerdmg=12
set speed=4
set mana=5
echo Blooblabloo
echo S
pause
goto home

:statdetermineBM
set playerdmg=5
set speed=6
set mana=8
goto home

:statdetermineayla
set playerdmg=7
set speed=7
set mana=4
echo I'm a butterfly, as sweet as can be. Please, can you buy some destruction for me?
pause
goto home

:statdetermineleon
set playerdmg=10
set speed=8
set mana=0
echo Viva La Chameleon!
pause
goto home

:statdeterminea
set playerdmg=5
set speed=9
set mana=6
goto home

:statdeterminew
set playerdmg=3
set speed=3
set mana=10
goto home

:statdeterminewa
set playerdmg=8
set speed=4
set mana=0
goto home

:statdeterminesam
set playerdmg=8
set speed=2
set mana=4
goto home

:statdeterminemag
set playerdmg=4
set speed=6
set mana=8
goto home

:SECRETDEVCLASS
set playerdmg=9999
set speed=9999
set mana=9999
echo SECRET DEV CLASS ACTIVATED
echo ALL STATS SET TO 9999
pause
goto home

:statdeterminenin
set playerdmg=3
set speed=10
set mana=5
goto home

:: ## YOUR HOME ##############################
:home
color 07
cls
echo.
echo Welcome, !name!.
echo You're in the city of !city!.
echo HP: !health!  $!money!
echo ATK: !playerdmg!
echo MANA: !mana!
echo SPD: !speed!
echo You are a !class!.
echo.
echo 1)Story Mode (WORK IN PROGRESS, NEARLY UNPLAYABLE)
echo 2)Go into the wild
echo 3)Shop
echo 4)Move to a New City
echo 5)Exit Game
echo 6)Save Game
echo 7)Load Game
echo 8)Music Club (WORK IN PROGRESS)
echo.
set /p c=
if "!c!" == "1" goto StoryModeBeg
if "!c!" == "2" goto PrePreBattle
if "!c!" == "3" goto shop
if "!c!" == "4" goto movehome
if "!c!" == "5" goto exitgame
if "!c!" == "6" goto savegame
if "!c!" == "7" goto loadgame
if "!c!" == "635883" goto derpearn
if "!c!" == "8" goto MusicClub
if "!c!" == "poo" goto easteregg1
goto home

:: I am going to do a poo..
:easteregg1
cls
echo You've gotta poo in your pants.
pause
goto home

:: ## EARNING MONEY.. the fast way.. #############################
:derpearn
set /a money+=10
goto home

:: ## LOADING A EXISTING GAME ################################
:loadgame
(
set /p health=
set /p playerdmg=
set /p money=
set /p bodycount=
set /p BB=
set /p Pistol=
set /p current=
set /p p=
set /p city=
set /p name=
set /p mana=
set /p class=
set /p speed=
)<save.sg
goto home

::## SAVING YOUR GAME ####################################
:savegame
(
echo !health!
echo !playerdmg!
echo !money!
echo !bodycount!
echo !BB!
echo !Pistol!
echo !current!
echo !p!
echo !city!
echo !name!
echo !mana!
echo !class!
echo !speed!
)>save.sg
goto home

::## MOVING YOUR HOME ####################################
:movehome
cls
echo You are moving to a new city, !name!
echo Enter the name of the place you want to move to.
set /p c=New City:
set city=!c!
goto home

::## PREMIUM GREETINGS! ###################################
:aboutpremium
cls
echo Thanks for buying Universe Premium.
echo With Universe Premium, you can type in a secret code at your home menu to get free money!
echo Your free money secret code is..
echo.
echo 635883
echo.
echo Type the code in at your home menu to get some free money. Eventually, you can stockpile tons of money to buy some epic stuff.
echo YOU MUST REMEMBER THIS CODE. If you don't, you will need to buy premium again.
pause
goto home

:aboutno1
cls
echo See.. you didn't want that, but you bought it anyway. Good job, buddy.
pause
exit

::## SHOP ################################################
:shop
cls
echo What would you like?
echo.
echo You have $!money!
echo.
echo 1) Sword of Wither [$10]
echo 2) HP Potion [$15]
echo 3) Muscle Potion [$25]
echo 4) Emerald Armor Set [$10000]
echo 5) Eye of Ender [$1000]
echo 6) Leather Armor Set [$50]
echo 7) A... Potato? [$1000]
echo 8) Toast with Butter [$5000]
echo 9) Universe Premium [$10000]
echo 10) You Don't Want This [$100000]
echo.
echo "b" to go back
echo.
set /p c=Enter:

if "!c!" == "1" goto purchase.BB
if "!c!" == "2" goto purchase.HP
if "!c!" == "3" goto purchase.MP
if "!c!" == "4" goto purchase.Pistol
if "!c!" == "5" goto purchase.EyeOfEnder
if "!c!" == "6" goto purchase.LeatherArmorSet
if "!c!" == "b" goto home
if "!c!" == "7" goto purchase.Potato
if "!c!" == "8" goto purchase.ToastWithButter
if "!c!" == "9" goto purchase.Premium
if "!c!" == "10" goto purchase.No1

:purchase.No1
set /a money-=100000
goto aboutno1

:purchase.Premium
set /a money-=10000
goto aboutpremium

:purchase.ToastWithButter
set /a money-=5000
set /a playerdmg+=3000
goto home

:purchase.LeatherArmorSet
set /a money-=50
set /a playerdmg+=5
goto home

:purchase.EyeOfEnder
set /a money-=1000
set /a playerdmg+=50
goto home

:purchase.MP
set /a money-=25
set /a playerdmg+=15
goto home

:purchase.BB
set /a money-=10
set /a BB=true
set /a playerdmg+=10
set /a p=true
goto home

:purchase.Pistol
set /a money-=10000
set /a pistol=true
set /a playerdmg+=300
set /a p=true
goto home

:purchase.HP
set /a money-=15
set /a health+=15
goto home

:purchase.Potato
set /a money-=1000
set /a playerdmg+=1337
goto home

::## ENCOUNTERS ###########################################
:PrePreBattle
goto PreBattle

:PreBattle

set monsterhealth=50
set /a monsterdmg=%random% %% 10+5
if !bodycount! GEQ 1 if !bodycount! LEQ 5 (
set /a monsterhealth+=10
set /a monsterdmg+=2
)
if !bodycount! GEQ 6 if !bodycount! LEQ 10 (
set /a monsterhealth+=20
set /a monsterdmg+=4
)
if !bodycount! GEQ 10 if !bodycount! LEQ 30 (
set /a monsterhealth+=40
set /a monsterdmg+=8
)
if !bodycount! GEQ 30 if !bodycount! LEQ 50 (
set /a monsterhealth+=50
set /a monsterdmg+=9
)
if !bodycount! GEQ 50 if !bodycount! LEQ 100 (
set /a monsterhealth+=70
set /a monsterdmg+=10
)
if !bodycount! GEQ 100 if !bodycount! LEQ 300 (
set /a monsterhealth+=100
set /a monsterdmg+=100
)
if !bodycount! GEQ 300
set /a monsterhealth+=250
set /a monsterdmg+=200
)

:encounter1
cls
color 24
echo A large red dragon encounters you!
echo             _____________
echo         ,:'.,            `-._
echo           `:.`---.__         `-._
echo             `:.     `--.         `.
echo               \.        `.         `.
echo       (,,(,    \.         `.   ____,-`.,
echo    (,'     `/   \.   ,--.___`.'
echo , ,'  ,--.  `,   \.;'         `
echo `{D, {    \  :    \;
echo   V,,'    /  /    //
echo   j;;    /  ,' ,-//.    ,---.      ,
echo   \;'   /  ,' /  _  \  /  _  \   ,'/
echo         \   `'  / \  `'  / \  `.' /
echo (Ryan W) `.___,'   `.__,'   `.__,' 
echo.
echo           You: %health%
echo           Them: %monsterhealth%
echo.
echo 1)Attack!
echo 2)Run away!
echo 3)Cast a spell!
echo.
set /p c=

if "!c!" == "1" goto attack1
if "!c!" == "2" goto run
if "!c!" == "3" goto spell1
goto encounter1

:run
if !speed! GEQ 5 goto home
echo Your too slow!
goto encounter1

:spell1
if !mana! LEQ 0 (
echo You try to cast a spell, but merely fart, and nothing happens!
ping localhost -n 5 >nul
goto encounter1
)
if !mana! GEQ 1 (
goto attack1
)

:attack1
echo You manage to hit the ferocious beast!
echo The dragon seems to be a bit angry.
goto dragonmation1
set /a health-=!monsterdmg!
set /a monsterhealth-=!playerdmg!
if !monsterhealth! lss 0 (
set /a money+=10
set /a playerdmg+=1
set /a mana+=2
set /a bodycount+=1
if !health! LEQ 0 (
goto DIE
)
goto continue1
)
goto encounter1

:dragonmation1
cls
echo             _____________
echo         ,:'.,            `-._
echo           `:.`---.__         `-._
echo             `:.     `--.         `.
echo               \.        `.         `.
echo       (,,(,    \.         `.   ____,-`.,
echo    (,'     `/   \.   ,--.___`.'
echo , ,'  ,--.  `,   \.;'         `
echo `{D, {    \  :    \;
echo   V,,'    /  /    //
echo   j;;    /  ,' ,-//.    ,---.      ,
echo   \;'   /  ,' /  _  \  /  _  \   ,'/
echo   X      \   `'  / \  `'  / \  `.' /
echo (Ryan W) `.___,'   `.__,'   `.__,' 
ping localhost -n 1 >nul
goto dragonmation2

:dragonmation2
cls
echo             _____________
echo         ,:'.,            `-._
echo           `:.`---.__         `-._
echo             `:.     `--.         `.
echo               \.        `.         `.
echo       (,,(,    \.         `.   ____,-`.,
echo    (,'     `/   \.   ,--.___`.'
echo , ,'  ,--.  `,   \.;'         `
echo `{D, {    \  :    \;
echo   V,,'    /  /    //
echo   j;;    /  ,' ,-//.    ,---.      ,
echo   \;'   /  ,' /  _  \  /  _  \   ,'/
echo  X X    \   `'  / \  `'  / \  `.' /
echo (Ryan W) `.___,'   `.__,'   `.__,' 
ping localhost -n 1 >nul
goto dragonmation3

:dragonmation3
cls
echo             _____________
echo         ,:'.,            `-._
echo           `:.`---.__         `-._
echo             `:.     `--.         `.
echo               \.        `.         `.
echo       (,,(,    \.         `.   ____,-`.,
echo    (,'     `/   \.   ,--.___`.'
echo , ,'  ,--.  `,   \.;'         `
echo `{D, {    \  :    \;
echo   V,,'    /  /    //
echo   j;;    /  ,' ,-//.    ,---.      ,
echo   \;'   /  ,' /  _  \  /  _  \   ,'/
echo X X X     \   `'  / \  `'  / \  `.'/
echo (Ryan W) `.___,'   `.__,'   `.__,' 
pause
goto attack2

:attack2
set /a health-=!monsterdmg!
set /a monsterhealth-=!playerdmg!
if !monsterhealth! lss 0 (
set /a money+=10
set /a playerdmg+=1
set /a mana+=2
set /a bodycount+=1
if !health! LEQ 0 (
goto DIE
)
goto continue1
)
goto encounter1

:continue1
cls
echo Do you wish to continue walking through the forest? (Y/N)
echo.
set /p c=Enter:

if "!c!" == "y" goto PreBattle
if "!c!" == "n" goto home
goto continue1

:congrats
cls
echo YOU WON THE BATTLE!
echo.
pause >nul
exit

:DIE
cls
color 07
echo You died..
echo Your adventure comes to an end, and although
echo it may be sad, a new adventure will sprout
echo from the ashes of this one.
pause >nul
goto menu

::  CREDITS
:Credits
cls
echo %|----------------------------------------------|
echo %|Programmed by Gabriel Ashcraft and Ryan Wilson%|
echo %|Idea By Gabriel Ashcraft                      %|
echo %|MusicClub Designed & Programmed by Ryan Wilson%|
echo %|Most art created by Gabriel Ashcraft          %|
echo %|Audio programmed into the game by Ryan Wilson %|
echo %|----------------------------------------------|
pause
goto menu

:StoryModeBeg
cls
echo In a world, where chaos reigns supreme...
echo Where bandits and thiefs get away...
echo there is no hope...
echo But...
echo What if there was hope?
echo What if there was a small, flickering light
echo of hope...
echo well...
echo What if you...
echo were that hope?
echo what if you could stop the reign of chaos?
echo stop the discord between the people
echo and slay the demons that forever
echo haunt the citizens of the towns and citys...
echo what if...
echo you...
echo were the chosen one?
echo this Universe holds great secrets
echo  ____________
echo /   *        \
echo |  *     *   |
echo |      *     |
echo |  * *    *  |
echo \____________/G.A.
echo and lots of loot...
echo Will you
echo find it first?
pause
cls
echo.
echo H       H HH    H HHHHHHH H     H HHHHHH HHH   HHH HHHHHH
echo H       H H H   H    H     H   H  H      H  H H    H
echo H       H H  H  H    H     H   H  HHHH   HHH   HH  HHHH
echo H       H H   H H    H      H H   H      H H     H H
echo HHHHHHHHH H    HH HHHHHHH    H    HHHHHH H  H HHH  HHHHHH
echo.
pause
cls
goto StoryModeSetup

:StoryModeSetup
echo You are walking down a path, in a forest
echo you can hear your !class! gear 
echo clunking about as you walk.
echo you can see the bright green leaves,
echo the fresh spring trees bringing fruit,
echo and the flowers of nature blossoming.
echo The smell of the spring pollen filled the air
echo the aroma was heavenly, the grass was as green as
echo can be...
echo when suddenly...
pause
echo someone shouted your name!
echo "Ryan" they shouted
echo you wonder to yourself "Who is that?!"
echo for you dont recognise the voice,
echo so they shouldent know your name
echo this was a strange event indeed.
echo Shall you investigate the strange voice? (Y/N)
set /p d=

if "!d!" = "y" set 1YOR=True
if "!d!" == "Y" set 1YOR=True
if "!d!" == "n" set 1YOR=False
if "!d!" == "N" set 1YOR=False

if "%1YOR%" == "True" goto 1Y
if "%1YOR%" == "False" goto 1N

::If they say Yes
:1Y
echo You run down the path, but take a de-tour
echo through the bushes and trees, going towards the voice
echo it shouts some more, "Ryan"! Ryan!" it repeats
pause
echo You get closer to the strange sound
echo "Ryan"!, it wouldent stop
echo your frantic, you want to know whats happening
echo is someone you used to know dieing?
echo are you hearing things?
echo all of these were to be answered in the next few moments
pause
echo You get to the source of the sound
echo It is a cave, at the bottom of a hill in the forest
echo you wonder "What the heck is going on?"
echo but a shadowy figure emerges from the cave
echo a short, but well built figure
echo it may even have some sort of hair...
pause
echo to your surpirse...
echo from the cave emerge two creatures..
pause
echo A TROLL AND A SIREN EMERGE FROM THE CAVE!
echo You think to yourself; "the siren must have been calling my name! Luring me to the cave!"
echo the troll looks battle-ready, it has a weird bloodlust to it
echo Theres too much foliage around to run away, you have to face it!
pause
cls
goto 1YF

::If they say No
:1N
echo You ignore the voices
echo and you continue down the road, still enjoying
echo the nature, the flowers, trees, birds and bees all seem
echo to be awake and happy. You find the entrance to a small village!
pause
goto 2SL

:1YF
goto 123123
:: SONIC THIS IS WHERE THE FIGHT GOES! 
::## ENCOUNTER OF THE TROLL AND SIREN - STORY MODE FIRST ENCOUNTER OGM ###########################################
:123123
goto SB1PRE

:SB1PRE

set monsterhealth=50
goto encounter12

:encounter12
cls
color 24
echo A Short but strong TROLL and magical SIREN emerge!
:: ASCII ART PLACEHOLDER
echo.
echo           You: %health%
echo           Troll and Siren: %monsterhealth%
echo.
echo 1)Attack!
echo 2)Run away!
echo 3)Cast a spell!
echo.
set /p c=

if "!c!" == "1" goto attack11
if "!c!" == "2" goto run11
if "!c!" == "3" goto spell11
goto encounter12

:run11
echo Theres too much foilage!
goto encounter12

:spell11
if !mana! LEQ 0 (
echo You try to cast a spell, but merely fart, and nothing happens!
goto encounter1
)
if !mana! GEQ 1 (
goto attack1
)

:attack1
echo You manage to hit them.
echo They seem to be angry.. be careful..
set /a health-=!monsterdmg!
set /a monsterhealth-=!playerdmg!
if !monsterhealth! lss 0 (
set /a money+=10
set /a playerdmg+=1
set /a mana+=2
set /a bodycount+=1
if !health! LEQ 0 (
goto DIE
)
goto continue12
)
goto encounter12

:continue12
cls
echo You manage to kill the siren and the troll.
echo You stroll down the road with your %class% gear clunking about..
echo ..and find the entrance to a small village!
echo You go inside..
pause
goto village1

:village1
cls
echo You are in the village of Arkans. The name reminds you of the word you need to say to cast one of your spells..
echo Many peasants are walking around happily.
echo There is a blacksmith forging an iron sword, with his hands darkened because of the ashes of the fires.
echo There is a church. It must be Sunday, because many well-dressed people are walking inside it.
echo There is a shrine with a healing stone that you can touch to be healed.
echo People walking by you notice your %class% gear and stare at it in awe.
echo.
echo 1)Go to the Church
echo 2)Go to the Blacksmith
echo 3)Go to the Shrine
echo 4)Leave the Village
set /p c=
if "%c%" == "1" goto church1
if "%c%" == "2" goto blacksmith1
if "%c%" == "3" goto shrine1
if "%c%" == "4" goto field1
goto village1

:church1
cls
echo You walk inside the church.
echo Everybody is quiet, except for the preacher, of course, since he is preaching.
echo The church visitors squint at you since you are breaking most of the silence with your gear.
echo The preacher stares at you with a stern look, and he stops preaching while studying you very carefully.
echo.
echo 1)Attack the Preacher
echo 2)Attack the Church Visitors
echo 3)Leave the Church
set /p c=
if "%c%" == "1" goto preacheratk1
if "%c%" == "2" goto churchatk1
if "%c%" == "3" goto village1
goto church1

:preacheratk1
cls
echo You run up to the preacher and get ready to attack him, but guards come and take you away.
echo.
echo You are being hanged in a dark cave-like room.
echo That night, you die of starvation.
pause
goto DIE

:churchatk1
cls
echo You run up to the church visitors and manage to slaughter a few of them, and instantly run away, shutting the doors behind you.
echo It appears that no one has seen your ruthless act, however, some people are staring at the blood on your gear, as you leave a trail of blood as you walk.
pause
goto village1

:field1
cls
echo You are in a large grassland.
echo Flowers are blooming all around you, and some bees seem to be drinking their pollen.
echo Many trees are around you, almost every single one holding a bee hive.
echo Then, you see guards near the entrance to a cave.
echo.
echo 1)Attempt to Go in the Cave
echo 2)Attack the Guards
echo 3)Go Back to Arkans
set /p c=
if "%c%" == "1" goto caveat1
if "%c%" == "2" goto guardatk1
if "%c%" == "3" goto village1
goto field1

:caveat1
cls
echo You try to go inside the cave, but the guards nudge you away from the cave.
echo They say that the cave has a dangerous unknown creature inside.
pause
goto field1

:guardatk1
cls
echo You run up to the guards to viciously kill them, and they challenge you to a fight!
pause
goto guardencounterpre1

::## ENCOUNTER OF THE FIRST CAVE GUARDS - STORY MODE FIRST ENCOUNTER OGM ###########################################
:guardencounterpre1
goto GE1PRE

:GE1PRE
set monsterhealth=50
goto guardencounter1

:guardencounter1
cls
color 24
echo Two guards get in their fighting positions!
:: ASCII ART PLACEHOLDER
echo.
echo           You: %health%
echo           Guards: %monsterhealth%
echo.
echo 1)Attack!
echo 2)Run away!
echo 3)Cast a spell!
echo.
set /p c=

if "!c!" == "1" goto attack11
if "!c!" == "2" goto run11
if "!c!" == "3" goto spell11
goto encounter12

:run11
echo They are too fast for you to run away from them! Plus, you want to get in that cave!
goto encounter12

:spell11
if !mana! LEQ 0 (
echo You try to cast a spell, but merely fart, and nothing happens!
goto encounter1
)
if !mana! GEQ 1 (
goto attack1
)

:attack1
echo You manage to hit the guards!
echo Blood splatters everywhere!
set /a health-=!monsterdmg!
set /a monsterhealth-=!playerdmg!
if !monsterhealth! lss 0 (
set /a money+=10
set /a playerdmg+=1
set /a mana+=2
set /a bodycount+=1
if !health! LEQ 0 (
goto DIE
)
goto guardbeat1
)
goto guardencounter1

:guardbeat1
cls
echo You kill the guards, and they fall to the ground, dead.
pause
goto cavein1

:cavein1
cls
echo You go inside the cave..
echo ..and see something lurking in the great darkness inside the cave..
pause
echo IT'S A LARGE DRAGON!
echo It burns some of your gear with its great red flames!
echo Wait.. what's the word to cast that spell?
set /p c=
if "%c%" == "Arkans" goto dragondefeat1
if "%c%" == "arkans" goto dragondefeat1
goto cavefail1

:cavefail1
echo You quickly run outside, as you are unprepared for this battle..
pause
goto field1

:dragondefeat1
echo That's it! The word was Arkans!
echo You cast the spell, and the dragon flies up with beams of light coming out of it, and it explodes into ashes.
pause
goto win1

:win1
cls
echo You have completed the unfinished story mode!
echo Well.. you didn't complete the full story mode.
echo You completed all that is in the story mode so far.
echo Just wait, and soon, you can try to tackle the full story mode!
pause
goto home